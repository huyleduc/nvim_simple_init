local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
    'shaunsingh/nord.nvim',
    {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.5',
        dependencies = { {'nvim-lua/plenary.nvim'} }
    },
    'nvim-treesitter/nvim-treesitter',
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { {'nvim-tree/nvim-web-devicons'} }
    },
    'nvim-treesitter/playground',

    --    'ThePrimeagen/harpoon',
    --    'mbbill/undotree',
    --    'tpope/vim-fugitive',
    --    'lervag/vimtex',
    --    'sainnhe/gruvbox-material',
    --    {'williamboman/mason.nvim'},
    --    {'williamboman/mason-lspconfig.nvim'},
    --
    --    {'VonHeikemen/lsp-zero.nvim', branch = 'v3.x'},
    --    {'neovim/nvim-lspconfig'},
    --    {'hrsh7th/cmp-nvim-lsp'},
    --    {'hrsh7th/nvim-cmp'},
    --    {'L3MON4D3/LuaSnip'},
    --    {
    --        "nvim-neorg/neorg",
    --        build = ":Neorg sync-parsers",
    --        dependencies = { "nvim-lua/plenary.nvim" },
    --    },
}

local opts = {}

require("lazy").setup(plugins, opts)
