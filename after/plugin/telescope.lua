local builtin = require('telescope.builtin')

local function createFindCommand(extensionExclusions, directoryExclusions)
  local command = {'find', '.', '-type', 'f'}

  -- Exclude specific file extensions
  for _, ext in ipairs(extensionExclusions) do
    local exclusion = {'-not', '-path', '*.' .. ext}
    command = vim.tbl_flatten({command, exclusion})
  end

  -- Exclude specific directories
  for _, dir in ipairs(directoryExclusions) do
    local exclusion = {'-not', '-path', '*/' .. dir .. '/*'}
    command = vim.tbl_flatten({command, exclusion})
  end

  return command
end

local extensionExclusions = {'pdf', 'jpg', 'png', 'pyc', 'out', 'aux'}
local directoryExclusions = {'node_modules'} -- Add the directories you want to exclude here

vim.keymap.set('n', '<leader>pf', function()
  builtin.find_files({
    find_command = createFindCommand(extensionExclusions, directoryExclusions)
  })
end, {})

vim.keymap.set('n', '<C-p>', function()
  builtin.git_files({
    find_command = createFindCommand(extensionExclusions, directoryExclusions)
  })
end, {})

vim.keymap.set('n', '<leader>ps', function()
  builtin.grep_string({ search = vim.fn.input("grep > ")})
end)

