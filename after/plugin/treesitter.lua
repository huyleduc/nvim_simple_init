local configs = require("nvim-treesitter.configs")

configs.setup({
    ensure_installed = { "javascript", "c", "lua",
        "vim", "vimdoc", "query", "vue", "typescript", "html", "css", "scss", "python"},
    sync_install = false,
    highlight = { enable = true },
    indent = { enable = true },
})

